package com.diego.proyecto.Util;

import com.diego.proyecto.GUI.Principal;
import com.diego.proyecto.Objetos.Empresa;
import com.diego.proyecto.Objetos.RRHH;
import com.diego.proyecto.Objetos.Trabajador;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**Clase para métodos
 * @author diego
 */
public class Util {

    //MEtodo con un mensaje de error
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }

    //Metodo para cargar un fichero de texto
    public static Object cargarFichero (String nombrefichero){
        //Se crea la lista y el inicio del objeto del fichero
        List<Trabajador> lista = null;
        ObjectInputStream deserializador = null;
        try{
            //Se carga el fichero y se lee el objeto.
            deserializador = new ObjectInputStream(new FileInputStream(nombrefichero));
            lista = (ArrayList<Trabajador>) deserializador.readObject();
        }catch (IOException ioe){

        }catch (ClassNotFoundException cnfe){

        }

        finally {
            if(deserializador != null){
                try{
                    deserializador.close();
                }catch (IOException ioe){

                }
            }
        }

        return lista;
    }
    public static final String PATH_EMPRESA = System.getProperty("user.home") + File.separator + "empresa.dat";
    //MEtodo para guardar un fichero binario
    public static void guardarFichero(Object objeto, String nombreFichero){
        //Se crea la variable del fichero
        ObjectOutputStream serializador = null;
        try{
            //Se crea el objeto del fichero
            serializador = new ObjectOutputStream(new FileOutputStream(nombreFichero));
            //Se escribe el objeto en un archivo de datos
            serializador.writeObject(objeto);
        }catch (IOException ioe){

        }

        if(serializador != null){
            try{
                serializador.close();
            }catch (IOException ioe){

            }
        }
    }
    //Metodos para exportar a XML (Los 3 funcionan igual)
    public static void XMLEmpresa(ArrayList<Empresa> vectorEmpresa){

        //Se crea la variable para el documento
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            //Se crea el documento XML
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            //Se crea el elemento raiz
            org.w3c.dom.Element raiz = documento.createElement("Empresas");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoEmpresa = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            //Se crea el nodo padre
            for (Empresa empresa : vectorEmpresa) { // Se recorren los vectores con un bucle.
                nodoEmpresa = documento.createElement("Empresa");
                raiz.appendChild(nodoEmpresa);
            //Se crean los nodos hijos
                nodoDatos = documento.createElement("nombre");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("tipo");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getTipo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("direccion");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getDireccion());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("telefono");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getTelefono());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("fundacion");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(empresa.getFundacion()));
                nodoDatos.appendChild(texto);
            }
            //Se guardan los datos en un documento XML
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File("empresa.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    public static void XMLTrabajador(ArrayList<Trabajador> vectorTrabajadores){

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            org.w3c.dom.Element raiz = documento.createElement("Trabajadores");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoTrabajador = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            for (Trabajador trabajador : vectorTrabajadores) {
                nodoTrabajador = documento.createElement("Trabajador");
                raiz.appendChild(nodoTrabajador);

                nodoDatos = documento.createElement("nombre");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("apellidos");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getApellidos());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("dni");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getDni());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("movil");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getMovil());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("contrato");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(trabajador.getContrato()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("empresa");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(trabajador.getEmpresa()));
                nodoDatos.appendChild(texto);

            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File("trabajadores.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }



    public static void XMLrrhh(ArrayList<RRHH> vectorRRHH){

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            org.w3c.dom.Element raiz = documento.createElement("Contratos");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoRRHH = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            for (RRHH rrhh : vectorRRHH) {
                nodoRRHH = documento.createElement("Contrato");
                raiz.appendChild(nodoRRHH);

                nodoDatos = documento.createElement("nombre");
                nodoRRHH.appendChild(nodoDatos);

                texto = documento.createTextNode(rrhh.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("categoria");
                nodoRRHH.appendChild(nodoDatos);

                texto = documento.createTextNode(rrhh.getCategoria());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("puesto");
                nodoRRHH.appendChild(nodoDatos);

                texto = documento.createTextNode(rrhh.getPuesto());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("horario");
                nodoRRHH.appendChild(nodoDatos);

                texto = documento.createTextNode(rrhh.getHorario());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("sueldo");
                nodoRRHH.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(rrhh.getSueldo()));
                nodoDatos.appendChild(texto);

            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File("contratos.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    public static void importarXMLr(){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            documento = builder.parse(new File("contratos.xml"));

            // Recorre cada uno de los nodos 'Persona'
            NodeList contrato = documento.getElementsByTagName("Contrato");
            for (int i = 0; i < contrato.getLength(); i++) {
                Node persona = contrato.item(i);
                Element elemento = (Element) persona ;
                Trabajador t = new Trabajador();

                t.setNombre(elemento.getElementsByTagName("nombre").item(0).
                        getChildNodes().item(0).getNodeValue());

                t.setApellidos(elemento.getElementsByTagName("apellidos").item(0).
                        getChildNodes().item(0).getNodeValue());

                t.setDni(elemento.getElementsByTagName("dni").item(0).
                        getChildNodes().item(0).getNodeValue());

                t.setDireccion(elemento.getElementsByTagName("direccion").item(0).
                        getChildNodes().item(0).getNodeValue());

                t.setEmpresa(elemento.getElementsByTagName("empresa").item(0).
                        getChildNodes().item(0).getNodeValue());

                t.setMovil(elemento.getElementsByTagName("movil").item(0).
                        getChildNodes().item(0).getNodeValue());

                t.setContrato(Date.valueOf(elemento.getElementsByTagName("contrato").item(0).
                        getChildNodes().item(0).getNodeValue()));


            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException saxe) {
            saxe.printStackTrace();
        }
    }

    public static void guardarArchivo(ArrayList array){
        JFileChooser jF1= new JFileChooser();
        String ruta = "";
        try{
            if(jF1.showSaveDialog(null)== JFileChooser.APPROVE_OPTION){
                ruta = jF1.getSelectedFile().getAbsolutePath() + ".dat";
                guardarFichero(array, ruta);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
