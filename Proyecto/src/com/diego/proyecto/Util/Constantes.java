package com.diego.proyecto.Util;

import java.io.File;

/**
 * Created by diego on 8/11/15.
 */
public class Constantes {

    //Constantes para los PATH de guardado
    public static final String PATH_EMPRESA = System.getProperty("user.home") + File.separator + "empresa.dat";
    public static final String PATH_TRABAJADOR = System.getProperty("user.home") + File.separator + "trabajadores.dat";
    public static final String PATH_RRHH = System.getProperty("user.home") + File.separator + "rrhh.dat";

}
