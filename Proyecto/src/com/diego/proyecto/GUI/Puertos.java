package com.diego.proyecto.GUI;

import com.diego.proyecto.Util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Puertos extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfNombrePuerto;
    private JTextField tfPuerto;
    private JPasswordField tfPasswordPuerto;

    private String usuario;
    private String contrasena;
    private String puerto;

    public Puertos() {
        super();
        setContentPane(contentPane);
        setTitle("Configuración");
        pack();
        setLocationRelativeTo(null);
        setModal(true);
        listeners();
    }

    public void listeners(){
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });
    }

    private void aceptar(){

        if((tfNombrePuerto.getText().equals(""))||(tfPasswordPuerto.equals(""))){
            JOptionPane.showMessageDialog(null, "Debes introducir usuario y contrase�a", "Login", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(tfPuerto.equals("")) tfPuerto.setText("3306");

        usuario = tfNombrePuerto.getText();
        contrasena = String.valueOf(tfPasswordPuerto);
        setVisible(false);

    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
