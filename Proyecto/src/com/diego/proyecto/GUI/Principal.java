package com.diego.proyecto.GUI;

import com.diego.proyecto.Objetos.Empresa;
import com.diego.proyecto.Objetos.RRHH;
import com.diego.proyecto.Objetos.Trabajador;
import com.diego.proyecto.Util.Util;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

import org.omg.CORBA.OBJECT_NOT_EXIST;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import static com.diego.proyecto.Util.Constantes.PATH_EMPRESA;
import static com.diego.proyecto.Util.Constantes.PATH_TRABAJADOR;
import static com.diego.proyecto.Util.Constantes.PATH_RRHH;

/**
 * Created by dos_6 on 28/10/2015.
 * Ventana princpal con contenido de variables, métodos y botones.
 * Añadidos aquellos necesarios de manera más directa para un mayor control
 * a primera vista
 */
public class Principal{

    //Declaración de variables
    private JPanel panel1;
    private JComboBox <String>cbCategoria;
    private JComboBox cbPuesto;
    private JButton btGuardarTrabajador;
    private JButton btBorrarTrabajador;
    private JTextField tfNombre;
    private JTextField tfApellidos;
    private JTextField tfDNI;
    private JTextField tfDireccion;
    private JTextField tfMovil;
    private JTextField tfSueldo;
    private JList <RRHH>jlistRRHH;
    private JTextField tfBusquedaRRHH;
    private JButton btBorrarRRHH;
    private JButton btGuardarRRHH;
    private JDateChooser jdateContrato;
    private JList <Trabajador>jlistTrabajador;
    private JButton btModificarTrabajador;
    private JButton btModifRRHH;
    private JTextField tfNombreEmpresa;
    private JTextField tfDireccionEmpresa;
    private JTextField tfTfnoEmpresa;
    private JTextField tfBusquedaEmpresa;
    private JList <Empresa>jlistEmpresa;
    private JDateChooser jdateEmpresa;
    private JComboBox cbTrabajador;
    private JButton btAnadirEmpresa;
    private JButton btModificarEmpresa;
    private JButton btBorrarEmpresa;
    private JTextField tfBusquedaTrabajador;
    private JComboBox cbNombreNomina;
    private JComboBox cbHorario;
    private JComboBox<String> comboBox1;
    private JLabel lbNombre;
    private JTabbedPane tpNomina;
    private JLabel lbEstado;
    private JButton cambiarPuertoButton;
    private JTable tablaEmpresa;
    private JTable tablaTrabajador;
    private JTable tablaRRHH;
    private JButton btRegistrar;
    private JButton btExportar;
    private JButton btNuevaEmpresa;
    private DefaultTableModel modeloTablaEmpresa;
    private DefaultTableModel modeloTablaTrabajador;
    private DefaultTableModel modeloTablaRRHH;

    public ArrayList<Trabajador> vectorTrabajadores;
    public ArrayList<RRHH> vectorRRHH;
    public ArrayList<Empresa> vectorEmpresa;

    private ArrayList<String> vectorNombresEmpresa;
    private ArrayList<String> vectorNombreTrabajadores;

    private int posicion = 0;

    private boolean error = true;
    private boolean modificar = false;
    private Connection conexion;

    //Constructor para iniciar los valores y archivos
    public Principal() {

        vectorNombresEmpresa = new ArrayList<>();
        vectorNombreTrabajadores = new ArrayList<>();


            login();
            System.out.println("Carga");




        initTablas();
        listarEmpresa();
        listarTrabajador();
        listarContrato();
        combosContrato();
        comboTrabajador();
        listeners();

    }

    private void initTablas(){
        String[] empresa = new String[]{"Nombre","Direccion","Telefono","Tipo","Fundacion"};
        String[] trabajador = new String[]{"Nombre","Apellidos","DNI","Direccion","Movil","Contrato","Empresa"};
        String[] contrato = new String[]{"Nombre", "Categoria", "Puesto", "Horario","Sueldo"};

        modeloTablaEmpresa = new DefaultTableModel();
        tablaEmpresa.setModel(modeloTablaEmpresa);
        modeloTablaEmpresa.addColumn("Nombre");
        modeloTablaEmpresa.addColumn("Direccion");
        modeloTablaEmpresa.addColumn("Telefono");
        modeloTablaEmpresa.addColumn("Tipo");
        modeloTablaEmpresa.addColumn("Fundacion");

        modeloTablaTrabajador = new DefaultTableModel();
        tablaTrabajador.setModel(modeloTablaTrabajador);
        modeloTablaTrabajador.addColumn("Nombre");
        modeloTablaTrabajador.addColumn("Apellidos");
        modeloTablaTrabajador.addColumn("DNI");
        modeloTablaTrabajador.addColumn("Direccion");
        modeloTablaTrabajador.addColumn("Movil");
        modeloTablaTrabajador.addColumn("Contrato");
        modeloTablaTrabajador.addColumn("Empresa");

        modeloTablaRRHH = new DefaultTableModel();
        tablaRRHH.setModel(modeloTablaRRHH);
        modeloTablaRRHH.addColumn("Nombre");
        modeloTablaRRHH.addColumn("Categoría");
        modeloTablaRRHH.addColumn("Puesto");
        modeloTablaRRHH.addColumn("Horario");
        modeloTablaRRHH.addColumn("Sueldo");


    }

    //Metodos para listar los diferentes vectores en las listas
    private void listarTrabajador(){

        String sql = "select * from trabajador";

        try {
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            cargarTrabajador(resultado);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void buscarTrabajador(String buscar){
        String sql = "select * from trabajador where nombre like"+ "\"%"+ buscar +"%\"";

        try {

            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            cargarTrabajador(resultado);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    private void comboTrabajador(){
        cbTrabajador.removeAllItems();
        for(String empresa:vectorNombresEmpresa){
            cbTrabajador.addItem(empresa);
        }
    }

    private void combosContrato(){
        cbNombreNomina.removeAllItems();
        for(String trabajador:vectorNombreTrabajadores){
            cbNombreNomina.addItem(trabajador);
        }
    }

    private void listarContrato(){
        String sql = "select * from rrhh";

        try {
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            cargarContratos(resultado);
            if(sentencia != null){
                sentencia.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void listarEmpresa(){
        String sql = "select * from empresa";

        try {
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);

            cargarEmpresas(resultado);
            if(sentencia != null){
                sentencia.close();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void cargarContratos(ResultSet resultado){
        modeloTablaRRHH.setNumRows(0);

        try {
            while(resultado.next()){
                Object[] fila = new Object[]{resultado.getString(2),resultado.getString(3),
                        resultado.getString(4),resultado.getString(5), resultado.getFloat(6)};
                modeloTablaRRHH.addRow(fila);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void cargarEmpresas(ResultSet resulado){
        modeloTablaEmpresa.setNumRows(0);
        vectorNombresEmpresa.clear();
        try {
            while(resulado.next()){
                Object[] fila = new Object[]{resulado.getString(2), resulado.getString(3),
                        resulado.getString(4), resulado.getString(5), resulado.getDate(6)};
                if(fila.length == 0){
                    modeloTablaEmpresa.setNumRows(0);
                }else{
                    vectorNombresEmpresa.add(resulado.getString(2));
                    modeloTablaEmpresa.addRow(fila);
                }

                            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void cargarTrabajador(ResultSet resultado){
        modeloTablaTrabajador.setNumRows(0);
        vectorNombreTrabajadores.clear();
        try {
            while(resultado.next()){
                try {
                    Object[] fila = new Object[]{resultado.getString(2), resultado.getString(3),
                            resultado.getString(4), resultado.getString(5), resultado.getString(6), resultado.getDate(7),
                            resultado.getString(8)};

                    vectorNombreTrabajadores.add(resultado.getString(2));
                    modeloTablaTrabajador.addRow(fila);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void buscarE(String buscar){

    String sql = "select * from empresa where nombre like"+ "\"%"+ buscar +"%\"";

        try {

            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            cargarEmpresas(resultado);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void buscarRRHH(String buscar){

        String sql = "select * from rrhh where nombre like"+ "\"%"+ buscar +"%\"";

        try {

            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            cargarContratos(resultado);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void listeners(){

        tablaTrabajador.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int i = tablaTrabajador.getSelectedRow();
                if(i == -1){
                    return;
                }
                mostrarTrabajador(i);
            }
        });

        tablaEmpresa.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int i = tablaEmpresa.getSelectedRow();
                if(i==-1){
                    return;
                }
                mostrarEmpresa(i);
            }
        });

        tablaRRHH.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int i = tablaRRHH.getSelectedRow();
                if(i == -1){
                    return;
                }
                mostrarContrato(i);
            }
        });

        tfBusquedaEmpresa.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(tfBusquedaEmpresa.getText().length() > 2){
                    buscarE(tfBusquedaEmpresa.getText());
                }else{
                    listarEmpresa();
                }
            }
        });

        tfBusquedaTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(tfBusquedaTrabajador.getText().length() > 2){
                    buscarTrabajador(tfBusquedaTrabajador.getText());
                }else{
                    listarTrabajador();
                }
            }
        });

        tfBusquedaRRHH.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(tfBusquedaRRHH.getText().length() > 2){
                    buscarRRHH(tfBusquedaRRHH.getText().toString());
                }else{
                    listarContrato();
                }
            }
        });

        btAnadirEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarEmpresa();
            }
        });

        btGuardarTrabajador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarTrabajador();
            }
        });

        btGuardarRRHH.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarContrato();
            }
        });

        btBorrarTrabajador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                borrarTrabajador();
                limpiarCajas();
            }
        });

        btBorrarEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                borrarEmpresa();
                limpiarCajas();
            }
        });

        btBorrarRRHH.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                borrarRRHH();
                limpiarCajas();
            }
        });

        btModificarEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificar = true;
                btModificarEmpresa.setEnabled(false);
            }
        });

        btModifRRHH.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificar = true;
                btModifRRHH.setEnabled(false);
            }
        });

        btModificarTrabajador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificar = true;
                btModificarTrabajador.setEnabled(false);
            }
        });

        btRegistrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrar();
            }
        });

        btExportar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Util.XMLEmpresa(getEmpresas());
                    Util.XMLTrabajador(getTrabajadores());
                    Util.XMLrrhh(getContratos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    public void mostrarTrabajador(int i){

        tfNombre.setText((String) tablaTrabajador.getValueAt(i,0));
        tfApellidos.setText((String)tablaTrabajador.getValueAt(i,1));
        tfDNI.setText((String) tablaTrabajador.getValueAt(i,2));
        tfDireccion.setText((String) tablaTrabajador.getValueAt(i,3));
        tfMovil.setText((String) tablaTrabajador.getValueAt(i,4));
        jdateContrato.setDate((java.util.Date) tablaTrabajador.getValueAt(i,5));
        cbTrabajador.setSelectedItem(tablaTrabajador.getValueAt(i,6));

    }

    public void mostrarEmpresa(int i){
        tfNombreEmpresa.setText((String) tablaEmpresa.getValueAt(i,0));
        tfDireccionEmpresa.setText((String)tablaEmpresa.getValueAt(i,1));
        tfTfnoEmpresa.setText((String) tablaEmpresa.getValueAt(i,2));
        comboBox1.setSelectedItem(tablaEmpresa.getValueAt(i,3));
        jdateEmpresa.setDate((java.util.Date) tablaEmpresa.getValueAt(i,4));
    }

    public void mostrarContrato(int i){
        cbNombreNomina.setSelectedItem(tablaRRHH.getValueAt(i,0));
        cbCategoria.setSelectedItem(tablaRRHH.getValueAt(i,1));
        cbPuesto.setSelectedItem(tablaRRHH.getValueAt(i,2));
        cbHorario.setSelectedItem(tablaRRHH.getValueAt(i,3));
        tfSueldo.setText(String.valueOf(tablaRRHH.getValueAt(i,4)));
    }

    private void borrarTrabajador(){
        int fila = 0;

        fila = tablaTrabajador.getSelectedRow();
        if(fila == -1){
            return;
        }
        String nombre = (String) tablaTrabajador.getValueAt(fila,0);
        String apellido = (String) tablaTrabajador.getValueAt(fila, 1);
       borrar("trabajador", "nombre", "apellidos", nombre, apellido);
        listarTrabajador();
        combosContrato();
    }

    private void borrarEmpresa(){
        int fila = 0;

        fila = tablaEmpresa.getSelectedRow();
        if(fila == -1) return;

        String nombre = (String) tablaEmpresa.getValueAt(fila, 0);
        String direccion = (String) tablaEmpresa.getValueAt(fila,1);
        borrar("empresa", "nombre", "direccion", nombre, direccion);
        listarEmpresa();
        comboTrabajador();
    }

    private void borrarRRHH(){
        int fila = 0;
        fila = tablaRRHH.getSelectedRow();
        if(fila == -1) return;

        String nombre = (String) tablaRRHH.getValueAt(fila, 0);
        String categoria = (String) tablaRRHH.getValueAt(fila, 1);
        borrar("rrhh","nombre","categoria", nombre, categoria);
        listarContrato();
    }

    private void borrar(String tabla, String p1, String p2, String res1, String res2){

        String sql = "delete from "+tabla+" where "+p1+" = ? and "+p2+" = ?";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1,res1);
            sentencia.setString(2,res2);
            sentencia.executeUpdate();
            if(sentencia != null){
                sentencia.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<RRHH> getContratos()
            throws SQLException {

        ArrayList<RRHH> contratos;

        String consulta = null;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        consulta = "SELECT * FROM rrhh";
        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();

        contratos = crearListaContrato(resultado);

        if (sentencia != null)
            sentencia.close();

        return contratos;
    }

    private ArrayList<RRHH> crearListaContrato(ResultSet resultado)
            throws SQLException {

        ArrayList<RRHH> contratos =
                new ArrayList<>();

        RRHH rrhh = null;
        while (resultado.next()) {

            rrhh = new RRHH();
            rrhh.setNombre(resultado.getString(2));
            rrhh.setCategoria(resultado.getString(3));
            rrhh.setPuesto(resultado.getString(4));
            rrhh.setHorario(resultado.getString(5));
            rrhh.setSueldo(Double.parseDouble(resultado.getString(6)));
            contratos.add(rrhh);
        }

        return contratos;
    }

    public ArrayList<Trabajador> getTrabajadores()
            throws SQLException {

        ArrayList<Trabajador> trabajador;

        String consulta = null;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        consulta = "SELECT * FROM trabajador";
        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();

        trabajador = crearListaTrabajador(resultado);

        if (sentencia != null)
            sentencia.close();

        return trabajador;
    }

    private ArrayList<Trabajador> crearListaTrabajador(ResultSet resultado)
            throws SQLException {

        ArrayList<Trabajador> trabajadores =
                new ArrayList<>();

        Trabajador trabajador = null;
        while (resultado.next()) {

            trabajador = new Trabajador();
            trabajador.setNombre(resultado.getString(2));
            trabajador.setApellidos(resultado.getString(3));
            trabajador.setDni(resultado.getString(4));
            trabajador.setDireccion(resultado.getString(5));
            trabajador.setMovil(resultado.getString(6));
            trabajador.setContrato(resultado.getDate(7));
            trabajador.setEmpresa(resultado.getString(8));
            trabajadores.add(trabajador);
        }

        return trabajadores;
    }

    public ArrayList<Empresa> getEmpresas()
            throws SQLException {

        ArrayList<Empresa> empresa;

        String consulta = null;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        consulta = "SELECT * FROM empresa";
        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();

        empresa = crearListaEmpresa(resultado);

        if (sentencia != null)
            sentencia.close();

        return empresa;
    }

    private ArrayList<Empresa> crearListaEmpresa(ResultSet resultado)
            throws SQLException {

        ArrayList<Empresa> empresas =
                new ArrayList<>();

        Empresa empresa = null;
        while (resultado.next()) {

            empresa = new Empresa();
            empresa.setNombre(resultado.getString(2));
            empresa.setDireccion(resultado.getString(4));
            empresa.setTelefono(resultado.getString(5));
            empresa.setTipo(resultado.getString(3));
            empresa.setFundacion(resultado.getDate(6));
            empresas.add(empresa);
        }

        return empresas;
    }

    //Metodos para importar XML(Los 3 funcionan del mismo modo)

    private void limpiarCajas(){
        tfNombreEmpresa.setText("");
        tfDireccionEmpresa.setText("");
        comboBox1.setSelectedItem(0);
        tfTfnoEmpresa.setText("");
        jdateEmpresa.setDate(null);

        tfNombre.setText("");
        tfApellidos.setText("");
        tfDNI.setText("");
        tfDireccion.setText("");
        tfMovil.setText("");
        cbTrabajador.setSelectedItem(0);
        jdateContrato.setDate(null);

        cbNombreNomina.setSelectedItem(0);
        cbPuesto.setSelectedItem(0);
        cbCategoria.setSelectedItem(0);
        cbHorario.setSelectedItem(0);
        tfSueldo.setText("0");
    }

    public void guardarTrabajador(){
        //Si hay algún campo vacío devuelve un mensaje de error.
        if(tfNombre.getText().equalsIgnoreCase("")||(tfApellidos.getText().equals(""))||tfDNI.getText().equals("")
                ||tfDireccion.getText().equals("")||(tfMovil.getText().equals(""))||(jdateContrato.getDate().equals(null))){

            Util.mensajeError("Introduce todos los datos");
            return;
        }
        if(tfMovil.getText().length()>9){
            Util.mensajeError("Error en el telefono, sobran caracteres");
            return;
        }else if(tfMovil.getText().length()<9){
            Util.mensajeError("Error en el telefono, faltan caracteres");
            return;
        }
        try{
            int numero = Integer.parseInt(tfMovil.getText());
        }catch (NumberFormatException nfe){
            Util.mensajeError("Error en el telefono");
            return;
        }
        if(modificar == false){
            String sql = "insert into trabajador (nombre, apellidos, dni, movil, direccion, contrato, empresa) values (?,?,?,?,?,?,?)";

            try {
                PreparedStatement sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1, tfNombre.getText());
                sentencia.setString(2, tfApellidos.getText());
                sentencia.setString(3, tfDNI.getText());
                sentencia.setString(4, tfMovil.getText());
                sentencia.setString(5, tfDireccion.getText());
                sentencia.setDate(6, new Date(jdateContrato.getDate().getTime()));
                sentencia.setString(7,cbTrabajador.getSelectedItem().toString());
                sentencia.executeUpdate();
                status("Alta con éxito");
            } catch (SQLException e) {
                Util.mensajeError("Error al dar de alta");
            }
        }else{
            int fila = tablaTrabajador.getSelectedRow();
            String nombre = (String) tablaTrabajador.getValueAt(fila, 0);
            String apellido = (String) tablaTrabajador.getValueAt(fila,1);

            String sql = "update trabajador set nombre = ?, apellidos = ?, dni = ?, direccion = ?, movil = ?, contrato = ?, empresa = ? where nombre = ?" +
                    " and apellidos = ?";

            try {
                PreparedStatement sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1, tfNombre.getText());
                sentencia.setString(2, tfApellidos.getText());
                sentencia.setString(3, tfDNI.getText());
                sentencia.setString(4, tfMovil.getText());
                sentencia.setString(5, tfDireccion.getText());
                sentencia.setDate(6, new Date(jdateContrato.getDate().getTime()));
                sentencia.setString(7,cbTrabajador.getSelectedItem().toString());
                sentencia.setString(8, nombre);
                sentencia.setString(9, apellido);
                sentencia.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }


            modificar = false;
            btModificarTrabajador.setEnabled(true);
        }


        listarTrabajador();
        combosContrato();
        limpiarCajas();

    }

    public void guardarEmpresa(){
        //Se controla el error para tener que rellenar todos los campos
        int numero = 0;
        if(tfNombreEmpresa.getText().equals("")||(tfDireccionEmpresa.getText().equals(" "))||
                (tfTfnoEmpresa.getText().equals(""))||(jdateEmpresa.getDate().equals(null))){
            Util.mensajeError("Rellena todos los campos");
            return;
        }
        if(tfTfnoEmpresa.getText().length()>9){
            Util.mensajeError("Error en el telefono, sobran caracteres");
            return;
        }else if(tfTfnoEmpresa.getText().length()<9){
            Util.mensajeError("Error en el telefono, faltan caracteres");
            return;
        }
        try{
            numero = Integer.parseInt(tfTfnoEmpresa.getText());
        }catch (NumberFormatException nfe){
            Util.mensajeError("Error en el telefono");
            return;
        }

        if(modificar == false){
            String sql = "insert into empresa (nombre, direccion, telefono, tipo, fundacion) values (?,?,?,?,?)";

            try {
                PreparedStatement sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1,tfNombreEmpresa.getText());
                sentencia.setString(2,tfDireccionEmpresa.getText());
                sentencia.setString(3, tfTfnoEmpresa.getText());
                sentencia.setString(4, comboBox1.getSelectedItem().toString());
                sentencia.setDate(5, new Date(jdateEmpresa.getDate().getTime()));
                sentencia.executeUpdate();
                status("Empresa registrada con éxito");
            } catch (SQLException e) {
                Util.mensajeError("Error al dar de alta una empresa");
                e.printStackTrace();
            }
        }else {
            int fila = tablaEmpresa.getSelectedRow();
            String nombreOriginal = (String) modeloTablaEmpresa.getValueAt(fila,0);
            String sql = "update empresa set nombre = ?, direccion = ?, telefono = ?" +
                    ", tipo = ?, fundacion = ? where nombre = ?";

            try {
                PreparedStatement sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1,tfNombreEmpresa.getText());
                sentencia.setString(2,tfDireccionEmpresa.getText());
                sentencia.setString(3, tfTfnoEmpresa.getText());
                sentencia.setString(4, comboBox1.getSelectedItem().toString());
                sentencia.setDate(5, new Date(jdateEmpresa.getDate().getTime()));
                sentencia.setString(6, nombreOriginal);
                sentencia.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            modificar = false;
            btModificarEmpresa.setEnabled(true);

            System.out.println(modificar);

        }
        listarEmpresa();
        comboTrabajador();
        limpiarCajas();
    }

    private void registrar(){
        Registro registro = new Registro();
        registro.setVisible(true);

        String nombre = registro.getUsuario();
        String pass = registro.getContrasena();
        String r = registro.getRol().toLowerCase();

        String sql = "insert into usuario (nombre, contrasena, rol) values (?,?,?)";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1,nombre);
            sentencia.setString(2, pass);
            sentencia.setString(3,r);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            Util.mensajeError("Fallo de registro");
            e.printStackTrace();
        }
        registro.setVisible(false);
    }

    public void guardarContrato(){
       //Si el sueldo se deja vacio se pone a 0
        if(tfSueldo.getText().equals("")) tfSueldo.setText("0");
        double sueldo=0;
        //Try-Catch para controlar que sólo haya numeros en el TextField de Sueldo
        try {
            sueldo = Float.parseFloat(tfSueldo.getText());
            error = false;
        } catch (NumberFormatException nfe) {
            Util.mensajeError("Error, introduce un número válido");
            return;
        }

        if(modificar==false){
            String sql = "insert into rrhh (nombre, categoria, puesto, horario, sueldo) values (?,?,?,?,?)";

            try {
                PreparedStatement sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1,cbNombreNomina.getSelectedItem().toString());
                sentencia.setString(2, cbCategoria.getSelectedItem().toString());
                sentencia.setString(3, cbPuesto.getSelectedItem().toString());
                sentencia.setString(4,cbHorario.getSelectedItem().toString());
                sentencia.setFloat(5, Float.parseFloat(tfSueldo.getText()));
                sentencia.executeUpdate();
                listarContrato();
            } catch (SQLException e) {
                Util.mensajeError("Error al registrar");
                e.printStackTrace();
            }
        }else{
            int fila = tablaRRHH.getSelectedRow();
            String nombreOriginal = (String) modeloTablaRRHH.getValueAt(fila,0);
            String categoriaOriginal = (String) modeloTablaRRHH.getValueAt(fila,1);

            String sql = "update rrhh set nombre = ?, categoria = ?, puesto = ?, horario = ?, sueldo = ? where nombre = ? and categoria = ?";

            try {
                PreparedStatement sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1,cbNombreNomina.getSelectedItem().toString());
                sentencia.setString(2, cbCategoria.getSelectedItem().toString());
                sentencia.setString(3, cbPuesto.getSelectedItem().toString());
                sentencia.setString(4,cbHorario.getSelectedItem().toString());
                sentencia.setFloat(5, Float.parseFloat(tfSueldo.getText()));
                sentencia.setString(6,nombreOriginal);
                sentencia.setString(7,categoriaOriginal);
                sentencia.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            modificar = false;
            btModifRRHH.setEnabled(true);

        }
        listarContrato();
        combosContrato();
        limpiarCajas();
    }

    private void status(String estado){
        lbEstado.setText(estado);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lbEstado.setText("");
    }

    private void conectar(String puerto,String nombre, String pass) throws ClassNotFoundException, SQLException{

        Class.forName("com.mysql.jdbc.Driver");
            /*
                fixme coger info de la conexion
                de un fichero de configuracion (.prop)
             */
        conexion = DriverManager.getConnection(
                "jdbc:mysql://localhost:"+puerto+"/trabajo", nombre, pass
        );
    }


    private void deshabilitar(boolean estado){
        tfNombreEmpresa.setEnabled(!estado);
        tfDireccionEmpresa.setEnabled(!estado);
        tfTfnoEmpresa.setEnabled(!estado);
        comboBox1.setEnabled(!estado);
        jdateEmpresa.setEnabled(!estado);

        tfNombre.setEnabled(!estado);
        tfApellidos.setEnabled(!estado);
        tfDNI.setEnabled(!estado);
        tfDireccion.setEnabled(!estado);
        tfMovil.setEnabled(!estado);
        jdateContrato.setEnabled(!estado);
        cbTrabajador.setEnabled(!estado);

        cbNombreNomina.setEnabled(!estado);
        cbCategoria.setEnabled(!estado);
        cbPuesto.setEnabled(!estado);
        cbHorario.setEnabled(!estado);
        tfSueldo.setEnabled(!estado);
    }

    private void login(){
        JLogin login = new JLogin();
        login.setVisible(true);

        String usuarios = login.getUsuario();
        String contrasena = login.getContrasena();
        String rol = login.getRol();

        String pass = login.getPassmysql();
        String nmsql = login.getNmysql();

        try {
            conectar("3306", nmsql, pass);
            System.out.println("Carga");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(usuarios);
        System.out.println(contrasena);
        System.out.println(rol);

        String sql = "SELECT * FROM usuario WHERE nombre = ? and contrasena = ? and rol = ?";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuarios);
            sentencia.setString(2, contrasena);
            sentencia.setString(3, rol);
            ResultSet resultado = sentencia.executeQuery();

            if(!resultado.next()){
                JOptionPane.showMessageDialog(null, "Datos incorrectos","Login", JOptionPane.ERROR_MESSAGE);
                login.setVisible(true);
                return;
            }else{
                if(usuarios.equalsIgnoreCase("root")){
                    JOptionPane.showMessageDialog(null, "Hail! Gran Lider", "Gran Logia", JOptionPane.WARNING_MESSAGE);
                }

            }

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }
        if(!rol.equalsIgnoreCase("root")){
            cambiarPuertoButton.setEnabled(false);
            btRegistrar.setEnabled(false);
            btAnadirEmpresa.setEnabled(false);
            btBorrarEmpresa.setEnabled(false);
            btModificarEmpresa.setEnabled(false);
            btGuardarTrabajador.setEnabled(false);
            btModificarTrabajador.setEnabled(false);
            btBorrarTrabajador.setEnabled(false);
            btGuardarRRHH.setEnabled(false);
            btModifRRHH.setEnabled(false);
            btBorrarRRHH.setEnabled(false);

            deshabilitar(true);
        }

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Principal");
        frame.setContentPane(new Principal().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(true);
        JMenuBar barraMenu = new JMenuBar();
        frame.setJMenuBar(barraMenu);
        frame.setSize(800,600);
    }
}
