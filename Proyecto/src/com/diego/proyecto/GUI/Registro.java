package com.diego.proyecto.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Registro extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfNombreRegistro;
    private JPasswordField tfPassRegistro;
    private JComboBox comboPrivilegios;

    private String usuario;
    private String contrasena;
    private String rol;

    public Registro() {
        setContentPane(contentPane);
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        setTitle("Registro");
        listeners();
    }

    private void listeners(){
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
    }

    private void aceptar(){
        if(tfNombreRegistro.getText().equals("") || tfPassRegistro.equals("")){
            JOptionPane.showMessageDialog(null, "Debes introducir usuario y contrase�a", "Registro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        usuario = tfNombreRegistro.getText();
        contrasena = tfPassRegistro.getText() ;
        rol = (comboPrivilegios.getSelectedItem().toString());
        setVisible(false);
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
