package com.diego.proyecto.GUI;

import com.diego.proyecto.Util.Util;
import sun.security.util.Password;

import javax.swing.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JLogin extends JDialog {
    private JPanel contentPane;
    private JButton btLogin;
    private JButton btCancelar;
    private JTextField tfNombre;
    private JPasswordField tfPassword;
    private JComboBox comboBox1;
    private JTextField tfNombresql;
    private JTextField tfpassMysql;

    private String usuario;
    private String contrasena;
    private String rol;
    private String nmysql;
    private String passmysql;

    private Connection conexion;

    public JLogin() {
        super();
        setContentPane(contentPane);
        setTitle("Login");
        pack();
        setLocationRelativeTo(null);
        setModal(true);
        listeners();

    }

    private void listeners(){
        btLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });

        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(3);
            }
        });

    }

    private void aceptar(){

        if((tfNombre.getText().equals(""))||(tfPassword.equals(""))){
            JOptionPane.showMessageDialog(null, "Debes introducir usuario y contrase�a", "Login", JOptionPane.ERROR_MESSAGE);
            return;
        }

        usuario = tfNombre.getText();
        contrasena = tfPassword.getText();
        rol = comboBox1.getSelectedItem().toString();
        passmysql = tfpassMysql.getText();
        nmysql = tfNombresql.getText();
        setVisible(false);
    }

    public String getNmysql() {
        return nmysql;
    }

    public void setNmysql(String nmysql) {
        this.nmysql = nmysql;
    }

    public String getPassmysql() {
        return passmysql;
    }

    public void setPassmysql(String passmysql) {
        this.passmysql = passmysql;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    private void cancelar(){
        setVisible(false);
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
