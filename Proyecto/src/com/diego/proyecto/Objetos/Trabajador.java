package com.diego.proyecto.Objetos;

import java.io.Serializable;
import java.util.Date;

/**Estructura de un objeto Trabajador
 * @author diego
 */
public class Trabajador implements Serializable{

    private static final long serialVersionUID = 1L;

    private String nombre;
    private String apellidos;
    private String dni;
    private String movil;
    private String direccion;
    private Date contrato;
    private String empresa;

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getContrato() {
        return contrato;
    }

    public void setContrato(Date contrato) {
        this.contrato = contrato;
    }

    public String toString(){
        return nombre +" "+apellidos;
    }
}
