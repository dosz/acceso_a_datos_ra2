package com.diego.proyecto.Objetos;

import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**Estructura de un objeto Empresa
 * @author diego
 */
public class Empresa implements Serializable{

    private static final long serialVersionUID = 1L;

    private String nombre;
    private String direccion;
    private String telefono;
    private String tipo;
    private Date fundacion;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFundacion() {
        return fundacion;
    }

    public void setFundacion(Date fundacion) {
        this.fundacion = fundacion;
    }

    public String toString(){
        return nombre+" "+tipo;
    }
}
