package com.diego.proyecto.Objetos;

import java.io.Serializable;

/**Estructura de un objeto de Contratos
 * Created by dos_6 on 01/11/2015.
 */
public class RRHH implements Serializable{

    private static final long serialVersionUID = 1L;
    private String nombre;
    private String categoria;
    private String puesto;
    private String horario;
    private double sueldo;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public String toString(){
        return categoria +" "+ puesto +" "+ nombre;
    }
}
