CREATE DATABASE IF NOT EXISTS trabajo;
USE trabajo;

CREATE TABLE IF NOT EXISTS usuario(

	id int unsigned auto_increment primary key,
	nombre varchar(50) not null unique,
	contrasena varchar(50) not null

);

CREATE TABLE IF NOT EXISTS empresa (

	id int unsigned auto_increment primary key,
	nombre varchar(50) not null,
	direccion varchar(50) not null,
	telefono varchar(9) not null,
	tipo varchar(20) not null,
	fundacion date not null

);

CREATE TABLE IF NOT EXISTS trabajador(

	id int unsigned auto_increment primary key,
	nombre varchar(50) not null,
	apellidos varchar(50) not null,
	dni varchar(9) not null,
	direccion varchar(50) not null,
	movil varchar(9) not null,
	contrato date not null,
	id_empresa int unsigned,
	INDEX (id_empresa),
	foreign key (id_empresa)
		references empresa (id)

);

CREATE TABLE IF NOT EXISTS rrhh(

	id int unsigned auto_increment primary key,
	nombre varchar(50) not null,
	categoria varchar(50) not null,
	puesto varchar(50) not null,
	horario varchar(50) not null,
	sueldo float not null default 0,
	id_trabajador int unsigned,
	INDEX (id_trabajador),
	foreign key (id_trabajador)
		references trabajador (id)

);
